const { execSync } = require("child_process");
const { opendirSync } = require("fs");
const path = require("path");

const ASSETS_DIR = (process.argv.length > 2) ? process.argv[2] : "content";
const DEST_DIR = (process.argv.length > 2) ? process.argv[3] : "content";

const DEST_EXT = process.env.DEST_EXT ? process.ENV.DEST_EXT : "webp";
const IMAGE_EXTS = ["jpg", "png", "webp"];

const MAGICK_BIN = process.env.MAGICK ? process.env.MAGICK : "convert";

const RESULT_HEIGHT = process.env.HEIGHT ? parseInt(process.env.HEIGHT) : 360;
const RESULT_WIDTH = process.env.WIDTH ? parseInt(process.env.WIDTH) : 640;

const CONVERT_SUBCOMMAND = `\\( -clone 0 -blur 0x9 -resize ${RESULT_WIDTH}x${RESULT_HEIGHT}\\\\! \\) \\( -clone 0 -resize ${RESULT_WIDTH}x${RESULT_HEIGHT} \\) -delete 0 -gravity center -compose over -composite`;


function* traverseDir(path) {
  var dir = opendirSync(path);
  var dirent = dir.readSync();

  while (dirent != null) {
    if (dirent.isDirectory())
      yield* traverseDir(dirent.path);

    else if (dirent.isFile())
      for (var ext of IMAGE_EXTS)
        if (dirent.path.endsWith(ext))
          yield dirent.path;

    dirent = dir.readSync();
  }
}


const createdDirs = {};
const images = traverseDir(ASSETS_DIR);

for (var image of images) {

  if (ASSETS_DIR != DEST_DIR) {
    var outimage = image.replace(ASSETS_DIR, DEST_DIR);
    outimage = outimage.split(".")[0] + "." + DEST_EXT;

    var dirname = path.dirname(outimage);
    if (createdDirs[dirname] != true) {
      var mkdir = `mkdir -p ${dirname}\n`
      console.log(mkdir)
      execSync(mkdir);
      createdDirs[dirname] = true;
    }
  } else outimage = image.split(".")[0] + "." + DEST_EXT;

  var command = `${MAGICK_BIN} ${image} ${CONVERT_SUBCOMMAND} ${outimage}\n`;

  try {
    console.log(command);
    execSync(command);
  }
  catch (error) {
    process.exit(1);
  }
}
