---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
summary: "Summary of the book."
tags: 
  - a
  - b
author: "Author"
link: "Link"
---

Book details.
