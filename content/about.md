# About Me

Note: If you need a formal description, then please check out my [resume](/resume).

I'm Arun Mani. You might me find me online with names J Arun Mani or Arun Mani J. Here _J_ is my [initial](https://en.wikipedia.org/wiki/Indian_name#Tamil). Please address me by my full name _Arun Mani_ or just _Arun_. Whatever you feel comfortable with 🙂.

My native is Tamil Nadu, India. I can speak Tamil (mother tongue!), English and can understand and speak basic Hindi.

I'm currently studying B.E Computer Science Engineering in Anna University, Regional Campus, Coimbatore. Apart from studies, I spend time reading books, coding and reading articles.

## Computer Science

I can't find a better name for this section as I think I'm not just _a programmer_. I mean, yes I'm one but not just one. 😉

I started coding at grade 9 (9th standard in Indian terms). For me, I was attracted to computers by video games. After a lot of web searches, I started with Python. Now after a few years, I know Python quite well. I also know basics of C, Java, JavaScript and Rust. My personal favorite is Python and Rust, because Python is simple and elegant and Rust has such a lovely compiler.

## Free Libre and Open Source

I'm an advocate of free libre and open source (FLOSS) technologies with respect for privacy. Most of the software I used are based on this opinion. But there are some things, both proprietary and privacy destroying, that I have to use because of various social constraints.

## Nerdy Cool Things

I use [Doom Emacs](https://github.com/hlissner/doom-emacs), a configuration kit for GNU Emacs. For operating system, I use [GNU Debian Linux](https://debian.org). Simplicity is key!

## Reading Preferences

I like books on fiction. Non-fiction is fine as long as it is not boring. I also love reading books on philosophy 🌟. I'm not an avid reader, but I try my best to complete at least one book a month. If you got a good book to share, don't forget me!

Reading blogs and articles is also my way of passing time. I try to read anything that is mildly interesting.

## Contact Me

Feel free to contact me for any feedback, comments and chit chat. For instant reply, use Telegram, for classic way, use email. The links are available in footer.
