---
title: "Recursion"
date: 2020-06-02T00:53:32+05:30
draft: false
summary: "First ever sci-fi novel I have ever read. As always time machines are dangerous."
tags:
  - fiction
  - sci-fi
  - time-travel
author: "Blake Couch"
link: "https://openlibrary.org/works/OL20073906W/Recursion"
---

First ever sci-fi novel I have ever read. As always time machines are dangerous.
