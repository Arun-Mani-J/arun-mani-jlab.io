---
title: "Hyperbole and a Half"
date: 2021-08-02T11:52:32+05:30
draft: false
summary: "Beautiful narration of various scenarios."
tags:
  - nonfiction
  - humor
  - narration
author: "Allie Brosh"
link: "https://openlibrary.org/works/OL16808806W/Hyperbole_and_a_Half"
---

Beautiful narration of various scenarios.
