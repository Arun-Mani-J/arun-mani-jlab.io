---
title: "Plato and a Platypus Walk Into a Bar: Understanding Philosophy Through Jokes"
date: 2024-01-30T21:38:14+05:30
draft: false
summary: "So all the jokes we loved are just questions from philosophy…"
tags: 
  - funny
  - humor
  - jokes
  - knowledge
  - philosophy
author: "Thomas Cathcart, Daniel Klein"
link: "https://www.goodreads.com/book/show/180995.Plato_and_a_Platypus_Walk_Into_a_Bar"
---

For a long time, I thought philosophy means having abstract vague questions that
are highly opinionated in nature and don't have a solid answer. It includes
questions like _what is the meaning of life?_, _is the action fair?_ etc. But
then I felt like - _okay, but what is there for someone to study it so
seriously?_. In short, I thought philosophy is a topic of conversation people
have from time to time in boring evenings where they themselves know that there
is no proper answer. _Sigh_.

Okay, turned out to be, philosophy is _kinda that_ but _not that_. I asked my
friend to give me practical examples of where philosophy is used. I asked her
to give a problem and explain how philosophy is used to solve it. She tried
explaining me that you can't understand philosophy just like that and yes, I
didn't.

I found out this book from a bookclub. The name sounded good and I felt like it
will help me understanding philosophy. Well, after reading I can say that the
book didn't help me with my original objective in anyway. But, at least, it told
me that philosophy is so vast and yea, spawns some long boring evenings and end
up confusing people like me.

So the book presents some topic and a lot of jokes. I expected the book to
explain the concept and then explain it with the jokes or at least explain the
joke with the concept. But, the book takes a lot of things for granted. There is
no real explanation actually. The book discusses a few paragraphs about the
topic and then presents jokes.

The jokes are popular too, some small differences and twists. So it is highly
probable that you know the jokes already.

In conclusion, if you already know or learning philosophy, then this book can be
a nice companion in the journey. But if like me, you are a fresher, then this
book may not be ideal, unless you wanted a book of jokes.
