---
title: "The Palace of Illusions"
date: 2024-02-18T16:53:23+05:30
draft: false
summary: "Mahabharata from Panchaali's eyes"
tags: 
  - fiction
  - hinduism
  - mahabharata
  - mythology
author: "Chitra Banerjee Divakaruni"
link: "https://www.goodreads.com/book/show/11456828-the-palace-of-illusions"
---

This book was gifted to me by my friend Levi from a book club for Christmas.
Thanks Levi.

The last few months, I have been reading a lot of books related to Mahabharata.
May be not so _a lot_ but _a lot_ at least. The same story is presented with
little variations and adjustments in different books and narrations. Nobody
knows which is the true one, if it ever exists.

So this book, it narrates you Mahabharata from [Draupadi
(Panchaali)](https://en.wikipedia.org/wiki/Draupadi)'s point of view. If you are
new to Mahabharata, then I suggest you to not start from this book. Read any
other Mahabharata book from a third-party perspective and then read this book.

This book? It is awesome. I read Mahabharata from [Jaya by Devdutt
Patnaik](/library/jaya), which tries to present a neutral narrative of what
happened with a list of variations at the end of each chapter. If you read [my
review for Jaya](/library/jaya), you will realize how I loved the book for its
short, to-the-point descriptions. But by sacrificing the adjectives, the words
lack any emotions.

However, The Palace of Illusions is completely opinionated. It speaks from
Draupadi's view. So there is a good amount of expressions and feelings preserved
inside the book. It also presents a cinematic style of stories where the twist
or surprise is at the last paragraph or sentence.

Overall, I loved the book for its presentation. If you are new to Mahabharata,
then some plots may not be so _obvious_ to you. That's why, this book is great
_once_ you know Mahabharata.
