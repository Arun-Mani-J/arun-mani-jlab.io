---
title: "The Kite Runner"
date: 2020-11-02T11:50:35+05:30
draft: false
summary: "Beautiful narration of a culture, traditions and difficulties."
tags:
  - fiction
  - friendship
  - culture
author: "Khaled Hosseini"
link: "https://openlibrary.org/books/OL37072845M/The_kite_runner"
---

Beautiful narration of a culture, traditions and difficulties.
