---
title: "The Midnight Library"
date: 2023-09-18T21:34:19+05:30
draft: false
summary: "Sometimes you think of your choices."
tags: 
  - death
  - depression
  - fiction
  - fantasy
  - parallel
  - physics
  - quantum 
author: "Matt Haig"
link: "https://www.goodreads.com/book/show/52578297-the-midnight-library"
---

Every moment in life, we have an infinite amount of choices though most of them
are hidden from our sight. This book is a reminder of the same.

General narration to story, everything was nice. The book has short chapters
which is what people like me love.

A non-spoiling gist of the story is it is about parallel world *kinda* life
where each choice branches to a different one. The protagonist gets to live most
of those different lives of her and realize herself.

I really like how this book has actual story instead of beating around the bush
with boring text on depression etc. That's a big plus.

Once you read the first few chapters, you will be able to predict the climax.
But still reading the book fully is needed to understand the *why* of the
ending.

In short, it is a very good story and a quick read too.
