---
title: "Norwegian Wood"
date: 2022-02-08T13:42:37+05:30
draft: false
summary: "A big love tale."
tags:
  - fiction
  - romance
  - contemporary
author: "Haruki Murakami"
link: "https://www.goodreads.com/book/show/17308035-norwegian-wood"
---

A big love tale.
