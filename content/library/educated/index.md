---
title: "Educated"
date: 2022-03-07T13:43:10+05:30
draft: false
summary: "Power of education for someone from a conservative family."
tags:
  - nonfiction
  - education
  - conservative
author: "Tara Westover"
link: "https://www.goodreads.com/book/show/36247169-educated"
---

Power of education for someone from a conservative family.
