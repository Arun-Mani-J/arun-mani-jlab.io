---
title: "Man's Search for Meaning"
date: 2020-09-02T11:50:22+05:30
draft: false
summary: "A lot of tough and difficult scenarios."
tags:
  - nonfiction
  - holocaust
  - narrative
author: "Viktor E. Franki"
link: "https://openlibrary.org/works/OL24557376W/Man%27s_Search_for_Meaning_adaptation"
---

A lot of tough and difficult scenarios.
