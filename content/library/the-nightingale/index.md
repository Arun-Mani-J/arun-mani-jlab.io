---
title: "The Nightingale"
date: 2023-07-15T14:26:26+05:30
draft: false
summary: "The war from the women's eyes and their heroism."
tags:
  - fiction
  - france
  - germany
  - war
  - women
  - world-war
author: "Kristin Hannah"
link: "https://www.goodreads.com/book/show/21853621-the-nightingale"
---

One of the worst things in _this world_ is being punished for your birth and
background to which you had no control over. This includes all sort of
discrimination based on your caste, color, race, name, family etc.

The next worst thing is suffering because of someone else's greed and being
pulled into trouble because _they_ think it is right. This includes all the wars
that have been taken millions of lives because of someone's ego, pride and
wrong-thinking.

I have already read [Man's Search for Meaning](/library/mans-search-for-meaning)
and it gave me an account of the horrors in the concentration camps.

This book was different. Assume we can divide the people into three groups during
war - those suffering by fighting for their country, those suffering by being
in prison and finally those left in their homes to suffer with the hell caused
by war. _The Nightingale_ belongs to the last category. It tells the story of
two sisters about their lives and acts during the World War II.

Though it is fiction, the book had a sense of realism to it. It is based on a
true war bought by true people which took true lives of innocents. The narration
was beautiful. I at sometimes, lost myself in thoughts on what I would do if a
war broke out. Or sometimes, I question what my action would be if I was in the
place of the protagonists. A war changes everything.

I loved reading the book. I had a goal of reading five chapters a day but soon
began to cross the limit and read as much as I can. I feel like the last
few chapters were rushed up and some more details could have been added to
ensure the smooth flow. Apart from that, this was a joyful reading of a past
nightmare.
