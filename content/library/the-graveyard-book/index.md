---
title: "The Graveyard Book"
date: 2023-08-03T19:42:54+05:30
draft: false
summary: "Interesting story of the dead among us."
tags:
  - fiction
  - graveyard
  - ghosts
author: "Neil Gaiman"
link: "https://www.goodreads.com/book/show/2213661.The_Graveyard_Book"
---

This book was suggested to me by friend Mangesh.

The title of the book itself was interesting and had an aroma of fantasy around
it. And as someone who loves fantasy like anything, I gave it a try.

The book has a very simple synopsis. A family is murdered by a skilled murderer
but a baby manages to escape. This baby finds care in a graveyard by the very
ghosts. But the murderer is still looking for the baby. So the story is all
about how the boy (the grownup baby) meets the murderer and learns some _really
twisty_ secrets about why the murderer is very keen on killing him.

With such a simple overview, you can get a _sorta_ story completion by reading
the first and last few chapters. The intermediate chapters feel like some random
happenings in the boy's life. BUT. Only at the climax you get to understand why
all these intermediaries were there in the first place. Each and everyone of
them has a small contribution at the last.

The book has a nice narration and so you won't be bored if you ready a chapter a
day.

What disappointed me is that the ending was a little vague. It just leaves you
incomplete without telling you _what's next?_, as if there is a sequel.

Apart from that, the book was good!
