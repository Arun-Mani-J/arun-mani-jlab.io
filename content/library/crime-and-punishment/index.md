---
title: "Crime and Punishment"
date: 2020-08-02T11:49:55+05:30
draft: false
summary: "I went full upside down reading this book. Perspective changing story."
tags:
  - fiction
  - crime
  - poverty
author: "Fyodor Dostoyevsky"
link: "https://openlibrary.org/works/OL31703044W/Crime_and_punishment"
---

I went full upside down reading this book. Perspective changing story.
