---
title: "One Hundred Years of Solitude"
date: 2022-05-02T14:10:45+05:30
draft: false
summary: "A brilliant narration of a family."
tags:
  - fiction
  - classic
  - magical-realism
author: "Gabriel García Márquez"
link: "https://www.goodreads.com/book/show/320.One_Hundred_Years_of_Solitude"
---

A brilliant narration of a family.
