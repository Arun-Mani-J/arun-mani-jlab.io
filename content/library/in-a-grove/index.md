---
title: "In a Grove"
date: 2021-07-02T11:53:15+05:30
draft: false
summary: "Awesome mystery surrounding a murder."
tags:
  - short-story
  - murder
  - mystery
author: "Ryūnosuke Akutagawa"
link: "https://www.goodreads.com/book/show/8132998-in-a-grove"
---

Awesome mystery surrounding a murder.
