---
title: "The Original Folk and Fairy Tales of the Brothers Grimm"
date: 2024-04-30T20:17:34+05:30
draft: false
summary: "The original dark version of the twisted tales."
tags:
  - dark
  - fairy
  - fantasy
  - folk
  - grimm
author: "Jacob Grimm, Wilhelm Grimm, Jack D. Zipe"
link: "https://www.goodreads.com/book/show/21981650-the-original-folk-and-fairy-tales-of-the-brothers-grimm"
---

I once saw a meme where you have two pictures side-by-side. The first one is a
_nice looking happy face_ with caption _The fairy tales we watch and read_. The
other picture is a _dark scary face_ with caption _Their German variant_. Sorry
for explaining a meme so badly but the crux is that the original German fairy
tales are not a _children-friendly happy-lovely_ one where you have animals
dancing, singing and enjoying with the princesses.

Since then, I always wanted to read the original variant but kept forgetting.
On one fine weekend, I was so bored. That time, I recalled the plan and
immediately looked for the book. The issue was, it was very difficult to get the
original version. Because there is _no_ original version. Fairy and folk tales
have a lot of variants and there is no definite authentic source.

Thankfully there was this book by Jack D. Zipe, who collected the first edition
of Grimm Fairy Tales and made small adjustments (I think?) in terms of language
etc. As usual with all the other types of book I read, this book also has small
stories with no ocean of adjectives.

Now coming to the contents of book itself. It was not that _dark_ as I expected
it to be like. There are some nasty punishments for villains or some tales don't
have a happy ending at all. A few stories are not like _hero-centered_. That is,
the story is there, just as a story. No plot line or anything.

I expected the stories to involve a lot of magic, a lot of talking animals and
creativity. But the stories, most of them followed a same template. For example,
when a girl goes in search of her prince (who promised to marry her), she goes
to the castle, gets a job there and tries to hint the prince of her presence by
singing every night.

But that doesn't mean the stories are boring. I loved the stories involving the
Devil. In that, the Devil helps a poor person (really!) in exchange of an
agreement like the person's soul etc. But at the end, the person outsmarts the
Devil or the Devil cancels the agreement and let the person live happily.

I also loved the stories which are like _X happened, that's why we have Y_. For
example, there are stories explaining why goats have shorter tails.

Then there are a few short single page stories that are so random.

Overall, I would say that this book is quite good if you are into fairy tales.
Though they might disappoint you, it is really nice to read the stories that
belong to a completely different era.
