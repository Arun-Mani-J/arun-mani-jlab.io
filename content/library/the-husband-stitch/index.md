---
title: "The Husband Stitch"
date: 2021-10-02T11:53:47+05:30
draft: false
summary: "A scary, interesting short story."
tags:
  - short-story
  - horror
  - fantasy
author: "Carmen Maria Machado"
link: "https://www.goodreads.com/book/show/55452104-the-husband-stitch"
---

A scary, interesting short story.
