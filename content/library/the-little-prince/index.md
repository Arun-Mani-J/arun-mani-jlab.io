---
title: "The Little Prince"
date: 2021-10-02T14:10:12+05:30
draft: false
summary: "A refreshing story that was fun to read."
tags:
  - fiction
  - fantasy
  - philosophy
author: "Antoine de Saint-Exupéry"
link: "https://www.goodreads.com/book/show/157993.The_Little_Prince"
---

A refreshing story that was fun to read.
