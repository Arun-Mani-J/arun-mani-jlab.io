---
title: "Looking for Alaska"
date: 2023-06-29T15:02:36+05:30
draft: false
summary: "A colorful love story"
tags:
  - fiction
  - love
  - depression
  - school-life
  - mystery
author: "John Green"
link: "https://www.goodreads.com/book/show/99561.Looking_for_Alaska"
---

I forgot this book completely. Yesterday at 2 AM, while thinking of completing
the [Malgudi Days](/library/malgudi-days), I suddenly got reminded of it.

So _only ~200 pages_? I can finish it.

Reminder to self - be careful when reading a love story 🥹.

This is really a paradoxical love mystery, and it reminded me of [Norwegian
Wood](/library/norwegian-wood). The narration was simple and a little more explicit with American slang.
Yea fine, Malgudi Days had Indian slang and so why not 😉.

The first part of the book was real fun. Full of events and happenings, it had
the visual feel attached to it.

The second half became a real slow wheel. Grinding the same thing again and
again and you feel like all the grinding was for nothing by the time you read
the last page. If this was an Indian film, then the second part would be two
songs and a 5 minutes scene.

But at least, the aroma of the first half, forces you to read the second. And
the first part is good enough to tickle you with memories or worry you with the
lack-of (_like me_) depending upon what kind of childhood you had and the school 😎.
