---
title: "Library"
---

I read books, watch movies and sometimes listen music. I try to review them here, or not a review but just a few words. The cover pictures are mostly taken from Open Library or Goodreads.
