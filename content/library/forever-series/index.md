---
title: "Forever Series"
date: 2021-07-02T14:09:21+05:30
draft: false
summary: "A modern love tale with suspense and mystery."
tags:
  - fiction
  - romance
  - thriller
author: "Novoneel Chakraborthy"
link: "https://www.goodreads.com/book/show/36067591-forever-is-a-lie"
---

A modern love tale with suspense and mystery.
