---
title: "The Girl on the Train"
date: 2020-07-02T01:01:12+05:30
draft: false
summary: "Fantastic thriller with a nice twist at the end."
tags:
  - fiction
  - psychological
  - suspense
author: "Paula Hawkins"
link: "https://openlibrary.org/works/OL18146683W/The_Girl_on_the_Train"
---

Fantastic thriller with a nice twist at the end.
