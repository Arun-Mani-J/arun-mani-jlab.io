---
title: "Malgudi Days"
date: 2023-06-29T14:55:03+05:30
draft: false
summary: "Heavy dose of day-to-day stories"
tags:
  - fiction
  - gossips
  - common-folk
  - stories
author: "R.K. Narayan"
link: "https://www.goodreads.com/book/show/14082.Malgudi_Days"
---

I had my exams this month, so I didn't read much, though I tried my best to read
one story a day.

Once you _ignore_ the foreword by some author (gosh it covered pages and did more
harm to me), this book is smooth.

The stories don't have any path, you just read and it finishes. So what kept me
reading? It is the _narration_.

It gives you the feel of a village folklore, of the daily happenings in your
neighborhood. Yes the story happened in the days where there was no phone or so
and yes I don't belong to that era. But born in a relatively remote area with a
parents of rich childhood memories, who here and there narrated me their
stories, Malgudi Days gave me the equivalent feel.

Reading through the stories without any objective felts like a usual gossiping.
You stand in your home and your neighbor comes and tell you of something in the
town. This book gave me that experience.

A few of the stories were interesting. A few simply left a sad impression, for
example the snake charmers, knife sharpeners, cobblers etc. The story described
the real life. No _hero_, no _rich mansion heroine_, no _action-romantic sequence_. It
was like a hard truth. Take it or forget it.

If you are someone from the Indian locality (not the urban region), then you
might get a strange nostalgia reading this book. At least, its stories are 10
pages roughly. So reading one a day won't be a big deal either.
