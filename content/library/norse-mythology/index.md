---
title: "Norse Mythology"
date: 2023-03-24T22:42:10+05:30
draft: false
summary: "An awesome collection of stories from Norse mythology."
tags:
  - fiction
  - mythology
  - norse
author: "Neil Gaiman"
link: "https://www.goodreads.com/book/show/37903770"
---

Honest disclosure, I'm a huge fan of fantasy and mythology, so I could be _over positive_ about this book.

After having a total failure on The Starless Sea, this book served as a big aid
in getting me back to reading™. I love mythology so crazily, and no matter how
much expressive they are, I always feel like that's not enough. But that's just
another story.

Now about this book- it was excellent. It has a little number of chapters where
each chapter is a short story involving Gods and giants. The stories were _really
short_, nothing verbose or boring. The number of pages in the book itself is
less, so a professional book dragon 🐉 can eat up from cover to cover within a
day.

It starts, like any other mythology story, with the narration of world creation.
After that a few stories about the famous Gods like Odin, Thor, Loki etc. The
middle stories are all about other Gods and other happenings. The final
chapters describes the _end_, which I guess, if I explain, would become a
spoiler.

What I liked more about the stories (or the Norse mythology itself) is that most
of the stories have a connection with the life we lead now. It could be about
cheating or wordplay or underestimation etc.

Finally, if you are in need of short stories of short duration to fill your
break time, then I highly recommend this book. You need not remember the
previous stories to read a new one as the stories are not much connected. So it
reduces the burden on your memory especially if you are a person like me who
forgets everything easily. 🤓

A great book, enjoyed every word of it.
