# Really Simple Syndication (RSS)

RSS stands for Really Simple Syndication. If you need to dive deep into the specification, then you should check out the [official website](https://www.rssboard.org/rss-specification). Here, I'm going to focus on the user's perspective. Please note that the article is not fool-proof, some mistakes surely exist. Please let me know, so I can update it.

## What is RSS?

RSS is a format by which you can access the updates to a website. The website developer, stores the changes in a file, for example in `www.example.org/index.xml`, the `index.xml` contains the changes made to the website. You on your end, will use an app known as [news aggregator](https://en.wikipedia.org/wiki/News_aggregator) to access the updates. So all you need to do is, install an aggregator app, add the website to its list. Now that app will let you know of the changes whenever it happens. The only requirement is that the developer of website has RSS enabled. Sounds simple right?

## Why not Newsletter?

Many _modern_ websites actually use newsletter, where you provide your email and they send you all the updates. Let's see what are its advantages and disadvantages.

### Advantages

- Emails can be personalized. The company can target your needs and draft the content in a way you might like. RSS on the other hand is plain and same for everyone. Though, some tweaks can be done, that is a headache.
- Delivery of an email is ensured (provided the email address is valid), which means the company knows that you actually received it (provided you haven't blocked it). But in RSS, it is you who has the control. If you decide to stop visiting a website, then all you need to do is just delete the link from your app. Emails require you to unsubscribe, where the company can collect your feedback and so.

### Disadvantages

- User is not in control. To let go off the updates, you need to unsubscribe from the company. It can be a big hassle.
- If the email addresses stored in the company database is breached, then it will be a threat to your privacy and security.
- Requires an email server. It can be costly, so not affordable by all.

## So RSS

The advantages of RSS are the disadvantages of the email. Full control for the user and no need of any email server. For an individual developer who is not any interested in user's usage (so called _tracking_), RSS is the best.

Many RSS aggregators exist, including free and open source ones. If you are on Android, you can check out [Feeder](https://gitlab.com/spacecowboy/Feeder). The steps to add a website to an app, depends on the app in particular.

## Conclusion

The article was meant to brief introduction to RSS. You can learn more about it from the world wild web. RSS is an old technology, it was once at the peak of usage. With the growth in personalized and targeted advertisements, it has gone to a decline. These days, especially among the personal blogs and websites, there is a growing interest in RSS because of privacy and control of user. Hope it continues and enriches.
