---
title: "DoNotRepeatBot"
date: 2023-01-02T00:49:54+05:30
draft: false
summary: "A Telegram bot to share messages quickly via inline."
tags:
  - bot
  - postgresql
  - ptb
  - python
  - repetition
  - telegram
repository: "https://github.com/arun-mani-j/DoNotRepeatBot"
---

|            |                                                                                                |
| ---------- | ---------------------------------------------------------------------------------------------- |
| Language   | Python                                                                                         |
| Repository | [https://github.com/arun-mani-j/DoNotRepeatBot](https://github.com/arun-mani-j/DoNotRepeatBot) |

DNR is a Telegram bot that allows you to share messages via [inline feature](https://core.telegram.org/bots/features#inline-requests) of Telegram. By storing repetitive messages in the bot, you can send the messages to any chat by calling the bot inline.

Initially, the bot worked only for text messages (so called _snippets_) but as days progressed, I rewrote the bot to allow any media type that Telegram allows to be sent via inline query. This means photos, audios, videos, documents and of course texts.

The version 2.0 also saw a new profile picture, designed by a Telegram user named Happy Princess, for the bot.

Personally I used to use this bot a lot to send messages in GNU/Linux groups where few questions liking mixing desktop environments, clarifications on WSL and normal distro are frequent.

The bot is written in [Python](https://www.python.org) using [python-telegram-bot](https://python-telegram-bot.org/) framework and [PostgreSQL](https://www.postgresql.org) is used as the database management system.

DNR was hosted on Heroku and available to users via [@donotrepeatbot](https://t.me/donotrepeatbot) bot account, but after Heroku decided to drop its [free plan](https://blog.heroku.com/next-chapter), the bot is inactive. However, DNR was a fun ride as it was the first Telegram bot I made which used inline queries.

**Update**
The bot is back alive thanks to [Suhasa SJ](https://suhasa.info/)!
