---
title: "Aashii"
date: 2023-01-02T00:50:11+05:30
draft: false
summary: "A Telegram bot to let members communicate with admins."
tags:
  - admins
  - bot
  - group
  - members
  - postgresql
  - ptb
  - python
  - telegram
repository: "https://github.com/arun-mani-j/Aashii"
---

|            |                                                                                |
| ---------- | ------------------------------------------------------------------------------ |
| Language   | Python                                                                         |
| Repository | [https://github.com/arun-mani-j/Aashii](https://github.com/arun-mani-j/Aashii) |

Aashii is a Telegram bot to simplify communication between members and admins of a group. Consider a Telegram group a lot of members and multiple admins. Here, the ability to let every user personally message an admin for any issue is a big trouble, both in terms of privacy and availability. To simplify this communication, Aashii is used.

Initially, all the admins of a group are added to a separate private group with Aashii as a member. Now, any member who wishes to contact the admins can contact the bot. The bot acts as a middle-hand forwarding messages in both directions.

In addition to message forwarding, Aashii supports multiple features like flood resistance, on the fly edits of messages, complete media support and broadcasting.

The bot is written in [Python](https://www.python.org) using [python-telegram-bot](https://python-telegram-bot.org/) framework and [PostgreSQL](https://www.postgresql.org) is used as the database management system.

Aashii is a self-hosted solution. It is currently being used by many Telegram users to provide customer service to their members.
