---
title: "Aayra"
date: 2023-01-02T00:50:24+05:30
draft: false
summary: "Read eBooks and listen audiobooks, together."
tags:
  - adwaita
  - audiobook
  - ebook
  - epub.js
  - ffmpeg
  - gstreamer
  - gtk
  - gui
  - mupdf
  - pdf.js
  - python
  - sqlite
repository: "https://gitlab.com/arun-mani-j/aayra"
---

|               |                                                                              |
| ------------- | ---------------------------------------------------------------------------- |
| Language      | Python                                                                       |
| Repository    | [https://gitlab.com/arun-mani-j/aayra](https://gitlab.com/arun-mani-j/aayra) |
| GUI Framework | GTK, Adwaita                                                                 |

Aayra is an eBook reader and audiobooks player. Its speciality is being able to do both of the functionality within the same app in a simplistic manner.

It uses [GTK](https://www.gtk.org) for GUI and [SQLite](https://www.sqlite.org) for database. The app is under heavy development, so you can keep track of the repository to know the changes.
