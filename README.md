# arun-mani-j.gitlab.io

This is my personal website made. It is made using [Hugo](https://gohugo.io) with [TailwindCSS](https://tailwindcss.com) and [daisyUI](https://daisyui.com).

## Development Setup

First install Hugo and [Node.js](https://nodejs.com).

Clone the repository to your desired location.

```sh
git clone git@gitlab.com:Arun-Mani-J/arun-mani-j.gitlab.io.git
```

Change your directory to the cloned repository and install the dependencies.

```sh
npm install
```

Run the CSS watcher (builds the stylesheets when they are changed)

```sh
npm run watch:css
```

Run the Hugo watcher (builds the website when the files are changed)

```sh
npm run watch:hugo
```

You should run the above two commands in two terminals simultaneously.

## Production Build

To build the website for production, use the following command.

```sh
npm run build
```

## License

Please check [LICENSE](/LICENSE).

## Acknowledgements

Thanks to developers of Hugo, TailwindCSS, daisyUI and many others involved for making the website possible.
Also thanks to the authors of external resources (mentioned in the license) for their media.
